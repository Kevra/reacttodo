import React, { Component } from 'react';

import './search-panel.css';

export default class SearchPanel extends Component {

	onSearchChange = (e) => {
		const searchString = e.target.value;
	    this.props.onItemSearch(searchString);
  	};

  	render() {
		return (
		  <input type="text"
		            className="form-control search-input"
		            placeholder="type to search"
		            onChange={this.onSearchChange} />
		);
	}
};
